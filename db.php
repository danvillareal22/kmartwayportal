<?php
global $CFG,$DB,$USER; 
class Kmart_Db{	
	
	//course completion
	public function get_course_completion($uid){

		global $DB, $CFG, $USER;

		require_once($CFG->dirroot . '/completion/cron.php');
		require_once($CFG->dirroot . '/lib/gradelib.php');

		ob_start();
		completion_cron($USER->id, 5); // Welcome to the Kmart Way
		completion_cron($USER->id, 9); // Customer Service
		ob_get_clean(); // Prevent logging from appearing in script!

		$coursecomplete = $DB->get_records_sql("SELECT CC.course as 'cid' FROM {course_completions} CC WHERE CC.course IN (5,6,7,10,8,9,13,14,11,16,12,17,19,18) AND CC.userid = ? AND CC.timecompleted IS NOT NULL", array($uid));
			return $coursecomplete;
	}
	
	public function get_user_course($uid){
		global $DB;
		$usercourses = enrol_get_users_courses($uid, true, NULL, 'timecreated DESC,sortorder ASC');
		
		foreach ($usercourses as $usercourse) {
					$cid[$usercourse->id] = $usercourse->id;
					}
		
		return $cid;
	}
}
?>