<?php 
/*echo $_SERVER['HTTP_USER_AGENT'] . "\n\n";
$browser = get_browser();
print_r($browser);
*/


error_reporting(0);
require_once('../config.php'); 
require_login();
include_once 'db.php';
include_once 'include/header.php';

$kl = new Kmart_Db();
//passing the value from the class(execute one query for all courses)
$cc = $kl->get_course_completion($USER->id);
//user course
$uc = $kl->get_user_course($USER->id);	

?>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0" >
            <div class="navbar-header" style="width:100%; background: url('images/header/header.jpg'); background-size: 100% auto; height:89px;" > 
			<button type="button" class="navbar-toggle" id="my-collaspible" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
            </button>
            <br>
			<span><h3 style="color: #000000; text-align:center; font-family:Roboto;" id="headtitle">My Induction</h3></span>
			
			          
            </div>

			<ul class="nav navbar-top-links navbar-right">
                <li class="mail" onclick='window.location.href="#"'> </li>
                <li class="home" onclick='window.location.href="<?php echo $CFG->wwwroot; ?>"'> </li>
               <li class="kart" onclick='window.location.href="#"'></li>
           </ul>
           
            <div class="navbar-default sidebar" role="navigation">
				<div class="row tabbable" style="margin-left:10px;">
					<div class="span3 fixme">
						<ul class="nav nav-pills nav-stacked">
							<li class="active tb1" id="tab-mandatory"><a href="#mandatory" data-toggle="tab"><br/ ><span style="font-size:20px; font-weight:bold;">My <br /> Induction<br /><br /></span></a></li>
							<li class="tb2" id="tab-optional"><a href="#optional" data-toggle="tab" style="padding-top:16px;"><span style="font-size:20px; font-weight:bold;">Delivering the Kmart Experience</span></a></li>
						</ul><br>
						 <img src="images/body/toggling1.png" id="toggling"/>
               
				</div>
         </div>

                <div class="sidebar-nav navbar-collapse" id="lap">
                    <ul class="nav" id="side-menu">
                        
                   
                    </ul>
                </div> 
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>
        <div id="page-wrapper">
            <div class="row">
<?php 
           echo html_writer::start_tag('span', array('style'=>'float:right;'));
           echo html_writer::empty_tag('br');
           echo "You are logged in as ".$USER->firstname." ".$USER->lastname." (<a href='".$CFG->wwwroot."/login/logout.php'>Logout</a>".")";
           echo html_writer::end_tag('span');
           ?>
                 <div class="tab-content" style="margin-left:13%;">
                <div id="mandatory" class="tab-pane active">
                    <br><br>                   
		<?php
		//activity group(Understanding Kmart)
		echo html_writer::start_tag('div',array('class'=>'activity_group'));
		echo html_writer::start_tag('h4',array('style'=>'color:#EA1D3C; font-family:roboto; font-size:1.5em;')).'Understanding Kmart'.html_writer::end_tag('h4'); 
		echo html_writer::empty_tag('br');
		//course id = 5
		if($uc[5]){
			if($cc[5]->cid==5){
				echo html_writer::start_tag('div',array('id'=>'5','class'=>'full-trolley_o'));
			}else{
				echo html_writer::start_tag('div',array('id'=>'5','class'=>'empty-trolley_o'));
			}
			echo html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br');
			echo html_writer::start_tag('div',array('class'=>'trolley-title_o'));
		}else{
			echo html_writer::start_tag('div',array('id'=>'5-disable','class'=>'fade-trolley_o'));
			echo html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br');
			echo html_writer::start_tag('div',array('class'=>'fade-trolley-title_o'));
		}
		
		echo 'Welcome to Kmart'; 
		echo html_writer::end_tag('div');
		echo html_writer::end_tag('div');
		
		//course id = 6
		if($uc[6]){
			if($cc[6]->cid==6){
			echo html_writer::start_tag('div',array('id'=>'6','class'=>'full-trolley_o'));
			}else{
			echo html_writer::start_tag('div',array('id'=>'6','class'=>'empty-trolley_o'));
			}
			echo html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br');
			echo html_writer::start_tag('div',array('class'=>'trolley-title_o'));
		}else{
			echo html_writer::start_tag('div',array('id'=>'6-disable','class'=>'fade-trolley_o'));
			echo html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br');
			echo html_writer::start_tag('div',array('class'=>'fade-trolley-title_o'));
		}
		
		echo 'Store Discovery'; 
		echo html_writer::end_tag('div');
		echo html_writer::end_tag('div');
		
		//course id = 7
		if($uc[7]){
			if($cc[7]->cid==7){
				echo html_writer::start_tag('div',array('id'=>'7','class'=>'full-trolley_o'));
			}else{
				echo html_writer::start_tag('div',array('id'=>'7','class'=>'empty-trolley_o'));
			}
			echo html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br');
			echo html_writer::start_tag('div',array('class'=>'trolley-title_o'));
		}else{
			echo html_writer::start_tag('div',array('id'=>'7-disable','class'=>'fade-trolley_o'));
			echo html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br');
			echo html_writer::start_tag('div',array('class'=>'fade-trolley-title_o'));
		}
		echo 'Working Safely'; 
		echo html_writer::end_tag('div');
		echo html_writer::end_tag('div');
		echo html_writer::end_tag('div');
		
		//activity group(Getting Started)
		echo html_writer::start_tag('div',array('class'=>'activity_group'));
		echo html_writer::start_tag('h4',array('style'=>'color:#EA1D3C; font-family:roboto; font-size:1.5em;')).'Getting Started'.html_writer::end_tag('h4'); 
		echo html_writer::empty_tag('br');
		//course id = 10
		if($uc[10]){
			if($cc[10]->cid==10){
				echo html_writer::start_tag('div',array('id'=>'10','class'=>'full-trolley_p'));
			}else{
				echo html_writer::start_tag('div',array('id'=>'10','class'=>'empty-trolley_p'));
			}
			echo html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br');
			echo html_writer::start_tag('div',array('class'=>'trolley-title_p'));
		}else{
			echo html_writer::start_tag('div',array('id'=>'10-disable','class'=>'fade-trolley_p'));
			echo html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br');
			echo html_writer::start_tag('div',array('class'=>'fade-trolley-title_p'));
		}
		echo 'Registers'; 
		echo html_writer::end_tag('div');
		echo html_writer::end_tag('div');
		
		//course id = 8
		if($uc[8]){
			if($cc[8]->cid==8){
				echo html_writer::start_tag('div',array('id'=>'8','class'=>'full-trolley_p'));
			}else{
				echo html_writer::start_tag('div',array('id'=>'8','class'=>'empty-trolley_p'));
			}
			echo html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br');
			echo html_writer::start_tag('div',array('class'=>'trolley-title_p1'));
		}else{
			echo html_writer::start_tag('div',array('id'=>'8-disable','class'=>'fade-trolley_p'));
			echo html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br');
			echo html_writer::start_tag('div',array('class'=>'fade-trolley-title_p1'));
		}
		echo 'Customer Service Tools'; 
		echo html_writer::end_tag('div');
		echo html_writer::end_tag('div');
		
		//course id = 9
		if($uc[9]){
			if($cc[9]->cid==9){
				echo html_writer::start_tag('div',array('id'=>'9','class'=>'full-trolley_p'));
			}else{
				echo html_writer::start_tag('div',array('id'=>'9','class'=>'empty-trolley_p'));
			}
			echo html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br');
			echo html_writer::start_tag('div',array('class'=>'trolley-title_pb'));
		}else{
			echo html_writer::start_tag('div',array('id'=>'9-disable','class'=>'fade-trolley_p'));
			echo html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br');
			echo html_writer::start_tag('div',array('class'=>'fade-trolley-title_pb'));
		}
		echo 'Customer Service'; 
		echo html_writer::end_tag('div');
		echo html_writer::end_tag('div');
		echo html_writer::end_tag('div');
		//echo html_writer::start_tag('span');
		echo html_writer::empty_tag('img',array('src'=>'images/body/prereq-arrow.png','id'=>'prereq-arrow'));
		//echo html_writer::end_tag('span');
		
		echo html_writer::start_tag('div',array('style'=>'clear:both')).html_writer::end_tag('div');
	
		//activity group(License to trade)
		echo html_writer::start_tag('div',array('class'=>'activity_group2'));
		echo html_writer::start_tag('h4',array('style'=>'color:#EA1D3C; font-family:roboto; font-size:1.5em;')).'License to Trade'.html_writer::end_tag('h4'); 
		echo html_writer::empty_tag('br');
		//course id = 12
		if($uc[170]){
			if($cc[170]->cid==170){
				echo html_writer::start_tag('div',array('id'=>'12','class'=>'full-trolley_blue'));
			}else{
				echo html_writer::start_tag('div',array('id'=>'12','class'=>'empty-trolley_blue'));
			}
			echo html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br');
			echo html_writer::start_tag('div',array('class'=>'fade-trolley-title_blue'));
		}else{
			echo html_writer::start_tag('div',array('id'=>'12-disable','class'=>'fade-trolley_blue'));
			echo html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br');
			echo html_writer::start_tag('div',array('class'=>'fade-trolley-title_blue'));
		}
		echo 'Code of Conduct';
		echo html_writer::end_tag('div');
		echo html_writer::end_tag('div');
		
		//course id = 17
		if($uc[225]){
			if($cc[225]->cid==225){
				echo html_writer::start_tag('div',array('id'=>'17','class'=>'full-trolley_blue'));
			}else{
				echo html_writer::start_tag('div',array('id'=>'17','class'=>'empty-trolley_blue'));
			}
			echo html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br');
			echo html_writer::start_tag('div',array('class'=>'trolley-title_blue'));
		}else{
			echo html_writer::start_tag('div',array('id'=>'17-disable','class'=>'fade-trolley_blue'));
			echo html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br');
			echo html_writer::start_tag('div',array('class'=>'fade-trolley-title_blue'));
		}
		echo 'Social Media';
		echo html_writer::end_tag('div');
		echo html_writer::end_tag('div');
			
		//course id = 19
		if($uc[475]){
			if($cc[475]->cid==475){
				echo html_writer::start_tag('div',array('id'=>'19','class'=>'full-trolley_blue'));
			}else{
				echo html_writer::start_tag('div',array('id'=>'19','class'=>'empty-trolley_blue'));
			}
			echo html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br');
			echo html_writer::start_tag('div',array('class'=>'trolley-title_blue'));
		}else{
			echo html_writer::start_tag('div',array('id'=>'19-disable','class'=>'fade-trolley_blue'));
			echo html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br');
			echo html_writer::start_tag('div',array('class'=>'fade-trolley-title_blue'));
		}
		echo 'Privacy';
		echo html_writer::end_tag('div');
		echo html_writer::end_tag('div');
		
		//course id = 18
		if($uc[220]){
			if($cc[220]->cid==220){
				echo html_writer::start_tag('div',array('id'=>'18','class'=>'full-trolley_blue'));
			}else{
				echo html_writer::start_tag('div',array('id'=>'18','class'=>'empty-trolley_blue'));
			}
			echo html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br');
			echo html_writer::start_tag('div',array('class'=>'trolley-title_blue'));
		}else{
			echo html_writer::start_tag('div',array('id'=>'18-disable','class'=>'fade-trolley_blue'));
			echo html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br');
			echo html_writer::start_tag('div',array('class'=>'fade-trolley-title_blue'));
		}
		echo 'Payment Credit Industry (PCI)';
		echo html_writer::end_tag('div');
		echo html_writer::end_tag('div');

		if($uc[478]){
			if($cc[478]->cid==478){
				echo html_writer::start_tag('div',array('id'=>'18','class'=>'full-trolley_blue'));
			}else{
				echo html_writer::start_tag('div',array('id'=>'18','class'=>'empty-trolley_blue'));
			}
			echo html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br');
			echo html_writer::start_tag('div',array('class'=>'trolley-title_blue'));
		}else{
			echo html_writer::start_tag('div',array('id'=>'18-disable','class'=>'fade-trolley_blue'));
			echo html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br');
			echo html_writer::start_tag('div',array('class'=>'fade-trolley-title_blue'));
		}
		echo 'Equal Employment Opportunity (EEO)';
		echo html_writer::end_tag('div');
		echo html_writer::end_tag('div');

		if($uc[223]){
			if($cc[223]->cid==223){
				echo html_writer::start_tag('div',array('id'=>'18','class'=>'full-trolley_blue'));
			}else{
				echo html_writer::start_tag('div',array('id'=>'18','class'=>'empty-trolley_blue'));
			}
			echo html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br');
			echo html_writer::start_tag('div',array('class'=>'trolley-title_blue'));
		}else{
			echo html_writer::start_tag('div',array('id'=>'18-disable','class'=>'fade-trolley_blue'));
			echo html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br');
			echo html_writer::start_tag('div',array('class'=>'fade-trolley-title_blue'));
		}
		echo 'Sale of Films and Video Games';
		echo html_writer::end_tag('div');
		echo html_writer::end_tag('div');

		if($uc[222]){
			if($cc[222]->cid==222){
				echo html_writer::start_tag('div',array('id'=>'18','class'=>'full-trolley_blue'));
			}else{
				echo html_writer::start_tag('div',array('id'=>'18','class'=>'empty-trolley_blue'));
			}
			echo html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br');
			echo html_writer::start_tag('div',array('class'=>'trolley-title_blue'));
		}else{
			echo html_writer::start_tag('div',array('id'=>'18-disable','class'=>'fade-trolley_blue'));
			echo html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br');
			echo html_writer::start_tag('div',array('class'=>'fade-trolley-title_blue'));
		}
		echo 'Automated Clearance and Kmart Resources';
		echo html_writer::end_tag('div');
		echo html_writer::end_tag('div');

		if($uc[18]){ 
			if($cc[18]->cid==18){
				echo html_writer::start_tag('div',array('id'=>'18','class'=>'full-trolley_blue'));
			}else{
				echo html_writer::start_tag('div',array('id'=>'18','class'=>'empty-trolley_blue'));
			}
			echo html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br');
			echo html_writer::start_tag('div',array('class'=>'trolley-title_blue'));
		}else{
			echo html_writer::start_tag('div',array('id'=>'18-disable','class'=>'fade-trolley_blue'));
			echo html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br');
			echo html_writer::start_tag('div',array('class'=>'fade-trolley-title_blue'));
		}
		echo 'Information Security';
		echo html_writer::end_tag('div');
		echo html_writer::end_tag('div');

		echo html_writer::end_tag('div');	
		echo html_writer::start_tag('div',array('class'=>'dropzone-container','style'=>'padding-right:10%'));
		echo html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('img',array('src'=>'images/body/kmart_shop.png','style'=>'border-radius:5px; position:fixed; bottom:0px; right:0px; z-index:0;','id'=>'droppable1'));
		echo html_writer::end_tag('div');
		
		echo html_writer::end_tag('div');
		?>
		
                <div id="optional" class="tab-pane">
                    <br><br>
					
		<?php
		//activity group(Filing and Presenting the Product)
		echo html_writer::start_tag('div',array('class'=>'activity_group'));
		echo html_writer::start_tag('h4',array('style'=>'color:#3399cc; font-family:roboto; font-size:1.5em;')).'Filling and Presenting the Product'.html_writer::end_tag('h4'); 
		echo html_writer::empty_tag('br');
		//course id = 13
		if($uc[13]){
			if($cc[13]->cid==13){
				echo html_writer::start_tag('div',array('id'=>'13','class'=>'full-trolley_pu'));
			}else{
				echo html_writer::start_tag('div',array('id'=>'13','class'=>'empty-trolley_pu'));
			}
			echo html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br');
			echo html_writer::start_tag('div',array('class'=>'trolley-title_pu'));
		}else{
			echo html_writer::start_tag('div',array('id'=>'13-disable','class'=>'fade-trolley_pu'));
			echo html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br');
			echo html_writer::start_tag('div',array('class'=>'fade-trolley-title_pu'));
		}
		echo 'Pricing and Ticketing'; 
		echo html_writer::end_tag('div');
		echo html_writer::end_tag('div');
		
		//course id = 14
		if($uc[14]){
			if($cc[14]->cid==14){
				echo html_writer::start_tag('div',array('id'=>'14','class'=>'full-trolley_pu'));
			}else{
				echo html_writer::start_tag('div',array('id'=>'14','class'=>'empty-trolley_pu'));
			}
			echo html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br');
			echo html_writer::start_tag('div',array('class'=>'trolley-title_pu1'));
		}else{
			echo html_writer::start_tag('div',array('id'=>'14-disable','class'=>'fade-trolley_pu'));
			echo html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br');
		    echo html_writer::start_tag('div',array('class'=>'fade-trolley-title_pu1'));
		}
		echo 'Store Presentation';
		echo html_writer::end_tag('div');
		echo html_writer::end_tag('div');
		
		//course id = 476
		if($uc[476]){
			if($cc[476]->cid==476){
				echo html_writer::start_tag('div',array('id'=>'476','class'=>'full-trolley_pu'));
			}else{
				echo html_writer::start_tag('div',array('id'=>'476','class'=>'empty-trolley_pu'));
			}
			echo html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br');
			echo html_writer::start_tag('div',array('class'=>'trolley-title_pu2'));
		}else{
			echo html_writer::start_tag('div',array('id'=>'476-disable','class'=>'fade-trolley_pu'));
			echo html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br');
			echo html_writer::start_tag('div',array('class'=>'fade-trolley-title_pu2'));
		}
		echo 'Preparing to Fill'; 
		echo html_writer::end_tag('div');
		echo html_writer::end_tag('div');
		
		//course id = 15
		if($uc[15]){
			if($cc[15]->cid==15){
				echo html_writer::start_tag('div',array('id'=>'15','class'=>'full-trolley_pu'));
			}else{
				echo html_writer::start_tag('div',array('id'=>'15','class'=>'empty-trolley_pu'));
			}
			echo html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br');
			echo html_writer::start_tag('div',array('class'=>'trolley-title_pu3'));
		}else{
			echo html_writer::start_tag('div',array('id'=>'15-disable','class'=>'fade-trolley_pu'));
			echo html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br');
			echo html_writer::start_tag('div',array('class'=>'fade-trolley-title_pu3'));
		}
		echo 'Filling Merchandise';
		echo html_writer::end_tag('div');
		echo html_writer::end_tag('div');
		echo html_writer::end_tag('div');
		
		//activity group(Delivering Fast and Friendly Service)
		echo html_writer::start_tag('div',array('class'=>'activity_group'));
		echo html_writer::start_tag('h4',array('style'=>'color:#3399cc; font-family:roboto; font-size:1.5em;')).'Delivering Fast and Friendly Service'.html_writer::end_tag('h4'); 
		echo html_writer::empty_tag('br');
		//course id = 11
		if($uc[11]){
			if($cc[11]->cid==11){
				echo html_writer::start_tag('div',array('id'=>'11','class'=>'full-trolley_b'));
			}else{
				echo html_writer::start_tag('div',array('id'=>'11','class'=>'empty-trolley_b'));
			}
			echo html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br');
			echo html_writer::start_tag('div',array('class'=>'trolley-title_b'));
		}else{
			echo html_writer::start_tag('div',array('id'=>'11-disable','class'=>'fade-trolley_b'));
			echo html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br');
			echo html_writer::start_tag('div',array('class'=>'fade-trolley-title_b'));
		}
		echo 'Customer Greeting';
		echo html_writer::end_tag('div');
		echo html_writer::end_tag('div');
		
		//course id = 16
		if($uc[16]){
			if($cc[16]->cid==16){
				echo html_writer::start_tag('div',array('id'=>'16','class'=>'full-trolley_b'));
			}else{
				echo html_writer::start_tag('div',array('id'=>'16','class'=>'empty-trolley_b'));
			}
			echo html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br');
			echo html_writer::start_tag('div',array('class'=>'trolley-title_b'));
		}else{
			echo html_writer::start_tag('div',array('id'=>'16-disable','class'=>'fade-trolley_b'));
			echo html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br');
			echo html_writer::start_tag('div',array('class'=>'fade-trolley-title_b'));
		}
		echo 'Self Serve Checkouts';
		echo html_writer::end_tag('div');
		echo html_writer::end_tag('div');
		echo html_writer::end_tag('div');

		echo html_writer::start_tag('div',array('style'=>'clear:both')).html_writer::end_tag('div');
		echo html_writer::start_tag('span');
		echo html_writer::empty_tag('img',array('src'=>'images/body/prereq-arrow2.png','style'=>'margin-top:6%; padding-left:7%; margin-left:60px; width:400px;'));
		echo html_writer::end_tag('span');
		echo html_writer::empty_tag('br');
		//activity group(Supporting our Customers)
		echo html_writer::start_tag('div',array('class'=>'activity_group'));
		echo html_writer::start_tag('h4',array('style'=>'color:#669933; font-family:roboto; font-size:1.5em;')).'Supporting our Clients'.html_writer::end_tag('h4'); 
		echo html_writer::empty_tag('br');
		//course id = 12
		if($uc[12]){
			if($cc[12]->cid==12){
				echo html_writer::start_tag('div',array('id'=>'12','class'=>'full-trolley_g'));
			}else{
				echo html_writer::start_tag('div',array('id'=>'12','class'=>'empty-trolley_g'));
			}
			echo html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br');
			echo html_writer::start_tag('div',array('class'=>'fade-trolley-title_g'));
		}else{
			echo html_writer::start_tag('div',array('id'=>'12-disable','class'=>'fade-trolley_g'));
			echo html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br');
			echo html_writer::start_tag('div',array('class'=>'fade-trolley-title_g'));
		}
		echo 'Fitting Rooms';
		echo html_writer::end_tag('div');
		echo html_writer::end_tag('div');
		
		//course id = 17
		if($uc[17]){
			if($cc[17]->cid==17){
				echo html_writer::start_tag('div',array('id'=>'17','class'=>'full-trolley_g'));
			}else{
				echo html_writer::start_tag('div',array('id'=>'17','class'=>'empty-trolley_g'));
			}
			echo html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br');
			echo html_writer::start_tag('div',array('class'=>'trolley-title_g'));
		}else{
			echo html_writer::start_tag('div',array('id'=>'17-disable','class'=>'fade-trolley_g'));
			echo html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br');
			echo html_writer::start_tag('div',array('class'=>'fade-trolley-title_g'));
		}
		echo 'Service Desk';
		echo html_writer::end_tag('div');
		echo html_writer::end_tag('div');
			
		//course id = 19
		if($uc[19]){
			if($cc[19]->cid==19){
				echo html_writer::start_tag('div',array('id'=>'19','class'=>'full-trolley_g'));
			}else{
				echo html_writer::start_tag('div',array('id'=>'19','class'=>'empty-trolley_g'));
			}
			echo html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br');
			echo html_writer::start_tag('div',array('class'=>'trolley-title_g'));
		}else{
			echo html_writer::start_tag('div',array('id'=>'19-disable','class'=>'fade-trolley_g'));
			echo html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br');
			echo html_writer::start_tag('div',array('class'=>'fade-trolley-title_g'));
		}
		echo 'Layby';
		echo html_writer::end_tag('div');
		echo html_writer::end_tag('div');
		
		//course id = 18
		if($uc[18]){
			if($cc[18]->cid==18){
				echo html_writer::start_tag('div',array('id'=>'18','class'=>'full-trolley_g'));
			}else{
				echo html_writer::start_tag('div',array('id'=>'18','class'=>'empty-trolley_g'));
			}
			echo html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br');
			echo html_writer::start_tag('div',array('class'=>'trolley-title_g'));
		}else{
			echo html_writer::start_tag('div',array('id'=>'18-disable','class'=>'fade-trolley_g'));
			echo html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('br');
			echo html_writer::start_tag('div',array('class'=>'fade-trolley-title_g'));
		}
		echo 'Entertainment and Imaging';
		echo html_writer::end_tag('div');
		echo html_writer::end_tag('div');
		echo html_writer::end_tag('div');		
		
		echo html_writer::start_tag('div',array('class'=>'dropzone-container','style'=>'padding-right:10%'));
		echo html_writer::empty_tag('br').html_writer::empty_tag('br').html_writer::empty_tag('img',array('src'=>'images/body/kmart_shop.png','style'=>'border-radius:5px; position:fixed; bottom:0px; right:0px; z-index:0;','id'=>'droppable2'));
		echo html_writer::end_tag('div');
		
		echo html_writer::end_tag('div');
		?>
                </div>                    
            </div><!--/.tab-content-->
          </div>  
        </div>
    </div>
<?php include('include/footer.php'); ?>
