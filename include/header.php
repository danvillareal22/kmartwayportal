<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>The Kmart Way</title>
    <link rel="stylesheet" href="css/kmart_desktop.css"/>
	<link rel="stylesheet" href="css/kmart.css"  media="only screen and (min-device-width : 800px) and (max-device-width: 1280px)"/>
	<link rel="stylesheet" href="css/kmart_ipad.css"  media="only screen and (min-device-width : 768px) and (max-device-width: 1024px)" />
    <link href="css/bootstrap/bootstrap.min.css" rel="stylesheet">
    <link href="css/metisMenu/metisMenu.min.css" rel="stylesheet">
	<link href="css/desktop.css" rel="stylesheet">
    <link href="css/galaxy.css" media="only screen and (min-device-width : 800px) and (max-device-width: 1280px)" rel="stylesheet">
    <link href="css/ipad.css" media="only screen and (min-device-width : 768px) and (max-device-width: 1024px)" rel="stylesheet">
    <link href="css/tablet.css" media="only screen and (min-device-width : 600px) and (max-device-width: 960px)" rel="stylesheet">
	<script src="js/jquery.min.js"></script>
    <script src="js/bootstrap/bootstrap.min.js"></script>
    <script src="js/metisMenu/metisMenu.min.js"></script>
    <script src="js/sb-admin-2.js"></script>
	<script type="text/javascript" src="js/kmart.js"></script>
	<script type="text/javascript" src="js/jquery-ui.js"></script>
	<script src="js/jquery.ui.touch-punch.min.js"></script>
	<script>
		touchMove = function(event) {
				event.preventDefault();
				draggable.style.left = touch.pageX-25 + 'px';
    			draggable.style.top = touch.pageY-25 + 'px';

		}
		ontouchmove="touchMove(event)"
        $(function() {
		  $('.tb2').css({
			  'background-image':'url(images/inactive_tab_BG.png)',
			  'background-repeat':'no-repeat'
			  
			  });
		  $('.tb1 span').css({
					'color': '#EA1D3C'
				});
		  $('.tb2 span').css({
			  'color': 'gray'
			});
		  $( "#5,#6,#7,#10,#8,#9,#13,#14,#476,#15,#11,#16,#12,#17,#19,#18,#170,#225,#475,#220,#478,#223,#222").draggable({
			  revert : 'invalid'
		  });   
		  $("#droppable1,#droppable2").droppable({
            drop: function( event, ui ) {
                console.log(ui);
                console.log(event);
                window.location.href = "<?php echo $CFG->wwwroot.'/course/view.php?id=';?>"+ui.draggable.attr("id");
				console.log(this);
				return false;
            }
          });
			$('#tab-mandatory').on('click',function(){
				$('#headtitle').text('My Induction');
				$('.tb2').css({
					'background-image':'url(images/body/inactive_tab_BG.png)',
					'background-repeat':'no-repeat'
				});
				
				$('.tb1 span').css({
					'color': '#EA1D3C'
				});
				$('.tb2 span').css({
					'color': 'gray'
				});
				
			});
			$('#tab-optional').on('click',function(){
				$('#headtitle').text('Delivering the Kmart Experience'); 
				$('.tb1').css({
					'background-image':'url(images/body/inactive_tab_BG.png)',
					'background-repeat':'no-repeat'
					});
				
				$('.tb1 span').css({
					'color': 'gray'
				});
				 $('.tb2 span').css({
					'color': '#337AB7'
				});
			
			});
			
        });
</script>
<?php 
include_once 'Mobile_Detect.php';
$detect = new Mobile_Detect;


?>
</head>
<body>